<?php

include_once 'database.php';

class Books {
    
    private $db;
    
    public function __construct() {
        $this->db = new Database();
    }
    
    public function getBooks($page, $order) {
        return $this->db->getAllBooks($page, $order);
    }
    
    public function getSingleBook($id) {
        return $this->db->getBook($id);
    }
    
    public function getPageCount() {
        return $this->db->getPageCount();
    }
    
    public function search($title) {
        return $this->db->searchByTitle($title);
    }
}

?>