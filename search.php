<!DOCTYPE html>
<html lang="lt">
    <head>
        <title>Knygynas</title>
        <link type="text/css" rel="stylesheet" href="web/bootstrap/css/bootstrap.min.css" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
    </head>
    <body>

        <div class="container">
            <div class="masthead">
                <ul class="nav nav-pills pull-right">
                    <form class="form-search" method="GET" action="search.php" name="search">
                        <input type="text" class="input-medium search-query" name="title" value="<?php 
                        if(isset($_GET['title'])) {
                            echo $_GET['title'];
                        };    
                        ?>">
                        <button type="submit" class="btn">Ieškoti</button>
                    </form>
                </ul>
                <h3 class="muted"><a href="index.php">Knygynas</a></h3>
            </div>
            <hr>

            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Pavadinimas</th>
                        <th>Metai</th>
                        <th>Autorius</th>
                        <th>Žanras</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php
                    include 'core\books.php';
                    
                    $books = new Books();
                    
                    if (isset($_GET['title'])) {
                        $list = $books->search($_GET['title']);
                    } else {
                        $list = $books->getBooks(1);
                    }
                    
                    foreach ($list as $book) {
                    ?>
                    <tr>
                        <td>
                            <a href="page.php?id=<?php echo $book['id']; ?>">
                                <?php echo $book['title']; ?>
                            </a>
                        </td>
                        <td><?php echo $book['release_date']; ?></td>
                        <td><?php echo $book['author']; ?></td>
                        <td><?php echo $book['genre']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
            <hr>

            <div class="footer">
                <p>© Armandas Dambrauskas | NFQ Akademija 2017</p>
            </div>

        </div> <!-- /container -->
        <script src="web/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>

