<!DOCTYPE html>
<html lang="lt">
    <head>
        <title>Knygynas</title>
        <link type="text/css" rel="stylesheet" href="web/bootstrap/css/bootstrap.min.css" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
    </head>
    <body>

        <div class="container">
            <div class="masthead">
                
                <ul class="nav nav-pills pull-right">
                    <form class="form-search" method="GET" action="search.php" name="search">
                        <input type="text" class="input-medium search-query" name="title">
                        <button type="submit" class="btn">Ieškoti</button>
                    </form>
                </ul>
                <h3 class="muted"><a href="">Knygynas</a></h3>
            </div>
            <hr>

            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Pavadinimas</th>
                        <th>Metai</th>
                        <th>Autorius</th>
                        <th>Žanras</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php
                    include 'core\books.php';

                    $order = '';
                    
                    if (isset($_GET['orderBy'])) {
                        $order = $_GET['orderBy'];
                    }
                    
                    if (isset($_GET['page'])) {
                        $page = (int)$_GET['page'];
                        if (empty($page)) {
                            $page = 1;
                        } 
                    } else {
                        $page = 1;
                    }
                    
                    $books = new Books();
                    $list = $books->getBooks($page, $order);
                    
                    foreach ($list as $book) {
                    ?>
                    <tr>
                        <td>
                            <a href="page.php?id=<?php echo $book['id']; ?>">
                                <?php echo $book['title']; ?>
                            </a>
                        </td>
                        <td><?php echo $book['release_date']; ?></td>
                        <td><?php echo $book['author']; ?></td>
                        <td><?php echo $book['genre']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">
                    <?php
                        $pages = $books->getPageCount();
                      
                        $linkOrder = '';
                        if (!empty($order)) {
                            $linkOrder .= '&orderBy='.$order;
                        }
                        
                        for ($i = 1; $i <= $pages; $i++) {
                    ?>
                    <a class="btn btn-mini" href="index.php?page=<?php echo $i.$linkOrder; ?>"><?php echo $i; ?></a>
                    <?php } ?>
                        </td>
                        <td colspan="2">
                            <div class="pull-right">
                                <form class="form-search" method="GET" action="index.php" name="order">
                                    <select name="orderBy">
                                        <option <?php if ($order === 'title') { ?>selected="selected"<?php } ?> value="title">Pagal pavadinimą</option>
                                        <option <?php if ($order === 'release_date') { ?>selected="selected"<?php } ?>  value="release_date">Pagal metus</option>
                                        <option <?php if ($order === 'author') { ?>selected="selected"<?php } ?>  value="author">Pagal autorių</option>
                                        <option <?php if ($order === 'genre') { ?>selected="selected"<?php } ?>  value="genre">Pagal žanrą</option>
                                    </select>
                                    <button type="submit" class="btn">Rikiuoti</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
            <hr>

            <div class="footer">
                <p>© Armandas Dambrauskas | NFQ Akademija 2017</p>
            </div>

        </div> <!-- /container -->
        <script src="web/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>

